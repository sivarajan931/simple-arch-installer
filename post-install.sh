#!/bin/bash
if [ $UID -eq 0 ]; then
    echo "Cannot be run as root."
    exit
fi


#Default .bashrc configuration of mine.
cp ./.bashrc ~/.bashrc
sudo cp .bashrc /root/.bashrc

#Install yay
echo "Installing Yay AUR helper..."
git clone https://aur.archlinux.org/yay.git ~/yay
cd ~/yay
makepkg -si --noconfirm

read -p "Do you like to add Chaotic AUR Repository (This repo contains most AUR packages that you do not needed to)?" -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key FBA220DFC880C036
    sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
    echo '[chaotic-aur]' | sudo tee -a /etc/pacman.conf
    echo 'Include = /etc/pacman.d/chaotic-mirrorlist' | sudo tee -a /etc/pacman.conf
    sudo pacman -Syy

fi

read -p "Do you want to install wine? [y/N]: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	sudo pacman -Syy
        sudo pacman -S --noconfirm --needed wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader
        yay -S --noconfirm dxvk-bin
fi

echo ""
read -p "Do you need Wireless Drivers? [y/N]: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman -S --noconfirm --needed wpa_supplicant iwd bluez bluez-utils pulseaudio-bluetooth bluez-firmware blueberry bluez-libs
    systemctl enable bluetooth.service
fi
echo ""
read -p "Do you need multimedia tools(blender, simplescreenrecorder, gimp, kdenlive)? [y/N]: " -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman -S --noconfirm --needed blender simplescreenrecorder gimp kdenlive
fi

echo ""
read -p "Do you need printer drivers? [y/N]" -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pacman -S --noconfirm --needed cups cups-pdf ghostscript gsfonts hplip system-config-printer
fi
# These packages will help you for trouble-shooting.
echo ""
echo "Installing downgrade, debtap..."
yay -S --noconfirm downgrade debtap

echo ""
echo "Do you need office software? 0. Do not install any office software, 1. Install libreoffice-fresh, 2. Install softmaker freeoffice(AUR) (non-free). 3. Install both."
echo "Your Choice: "
read n
if [ $n -eq 0 ]; then
    echo "Skipping..."
elif [ $n -eq 1 ]; then
    yay -S --noconfirm --needed libreoffice-fresh
elif [ $n -eq 2 ]; then
    yay -S --noconfirm --needed freeoffice
elif [ $n -eq 4 ]; then
    yay -S --noconfirm --needed freeoffice libreoffice-fresh
fi
echo ""
echo "Installing MS fonts for compatablity..."
yay -S -noconfirm --needed tff-ms-fonts

echo "Do you need octopi GUI package manager?[y/N]: "
if [[ $REPLY =~ ^[Yy]$ ]]
then
    yay -S --noconfirm octopi
fi

read -p "Do you need to configure powerlevel10k? [y/N] " -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
    yay -S --noconfirm ttf-meslo ttf-meslo-nerd-font-powerlevel10k zsh-theme-powerlevel10k-git
    cp ./.zshrc ~/.zshrc
    sudo cp .zshrc /root/.zshrc
    #cp .p10k.zsh ~/.p10k.zsh
    #sudo cp .p10k.zsh ~/.p10k.zsh
fi

echo ""

read -p "Do you need to configure LSP servers? [y/N]: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  yay -S java-language-server llvm clang bash-language-server --noconfirm --needed
fi


read -p "Do you need Tela Icon pack and archlinux-grub2-theme? [y/N]: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
  yay -S --noconfirm tela-icon-theme grub2-arch-theme
fi
echo "Do you need docker? (Docker used for software development.) [y/N]: "
if [[ $REPLY =~ ^[Yy]$ ]]
then
  sudo pacman -S docker docker-compose --noconfirm
  echo "Enabling docker service..."
  sudo systemctl enable docker.service docker.socket

fi

echo "Enter the custom packages to be installed in the file (even AUR packages too.)"
nano target

yay -S --noconfirm --needed $(awk '{print $1}'  target)


echo  "Thank you for using this script. If you get any errors pls troubleshoot and notify the fix to me."


